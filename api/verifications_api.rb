require 'json'


MyApp.add_route('POST', '', {
  "resourcePath" => "/verifications",
  "summary" => "Send Verification Email for newly created orgranization",
  "nickname" => "verifications_post", 
  "responseClass" => "EmailSendingStatus", 
  "endpoint" => "", 
  "notes" => "When a user registers a new organization, we need to send an email to the user's\nemail address to verify that it is the real email of the user.\nIn the email, there will be a link to come back to the Organization Home Page\nwith confirmation about the organization activation.",
  "parameters" => [
    {
      "name" => "body",
      "description" => "Organization name, Email address of the recipient and comeback URL.",
      "dataType" => "EmailVerification",
      "paramType" => "body",
    }
    
    ]}) do
  cross_origin

  body = request.body.read
  data = JSON.parse(body)

  options = { 
              'to' => data['recipient'], 
              'subject' => 'Verification Email', 
              'comeback_url' => data['comeback_url'],
              'organization_name' =>  data['organization_name']
            }

  result = VerificationMailer.send_mail(options)
  Pry::ColorPrinter.pp result

  { status: :success, message: "Verification sent to #{data['recipient']}"}.to_json
end
