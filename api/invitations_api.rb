require 'json'


MyApp.add_route('POST', '', {
  "resourcePath" => "/invitations",
  "summary" => "Send Invitation Email from an orgranization to users.",
  "nickname" => "invitations_post", 
  "responseClass" => "EmailSendingStatus", 
  "endpoint" => "", 
  "notes" => "When a user in an organization send invitation to another person,\nwe need to send an email to the invitee's email address to inform him.\nIn the email, there will be a link to come back to the Organization Home Page\nto accept the invitation.",
   "parameters" => [
    {
      "name" => "body",
      "description" => "Organization name, Email address of the recipient and comeback URL.",
      "dataType" => "EmailVerification",
      "paramType" => "body",
    }
    
    ]}) do
  cross_origin

  body = request.body.read
  data = JSON.parse(body)

  options = { 
              'to' => data['recipient'], 
              'subject' => 'Invitation Email', 
              'comeback_url' => data['comeback_url'],
              'organization_name' =>  data['organization_name']
            }

  result = InvitationMailer.send_mail(options)
  Pry::ColorPrinter.pp result

  { status: :success, message: "InvitationMailer sent to #{data['recipient']}"}.to_json
end

