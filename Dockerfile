FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y curl
RUN apt-get install -y git
RUN apt-get install -y vim
RUN apt-get install -y sendmail
RUN gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
RUN \curl -sSL https://get.rvm.io | bash -s stable
RUN echo "source /etc/profile.d/rvm.sh" >> /root/.bash_profile
RUN /bin/bash -l -c "rvm requirements"
RUN /bin/bash -l -c "rvm install 2.2.3 -- --enable-shared"

RUN mkdir projects
RUN cd /projects && git clone https://github.com/linhchauatl/sinatra-email-services-server.git
RUN /bin/bash -l -c "cd /projects/sinatra-email-services-server/ && gem install bundler && bundle"

RUN echo "ActionMailer::Base.raise_delivery_errors = true" >> /projects/sinatra-email-services-server/config/initializers/setup_email.rb
RUN echo "ActionMailer::Base.delivery_method = :sendmail" >> /projects/sinatra-email-services-server/config/initializers/setup_email.rb
RUN echo "ActionMailer::Base.view_paths = File.join('./app', 'views')" >> /projects/sinatra-email-services-server/config/initializers/setup_email.rb
RUN echo "cisco.com" >> /etc/mail/local-host-names

ENTRYPOINT /bin/bash -l -c "/etc/init.d/sendmail restart && cd /projects/sinatra-email-services-server && puma -e production -C ./puma.rb -p 3003 -d > puma.log && /bin/bash"
EXPOSE 3003