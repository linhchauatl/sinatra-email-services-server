require './lib/swaggering'

# only need to extend if you want special configuration!
class MyApp < Swaggering
  self.configure do |config|
    config.api_version = '1.0.0'
    Dir.glob('./api/**/*.rb').each { |rb_file| puts rb_file; require rb_file }
  end

  get '/api_docs' do
    api_docs_location = "#{Swaggering.base_path(request)}/resources.json"
    erb = ERB.new(File.read("./public/index.erb").untaint, 1)
    erb.result(OpenStruct.new({ api_docs_location: api_docs_location }).instance_eval { binding })
  end

  get %r{/public/(\w+)} do
    #Pry::ColorPrinter.pp request.env['REQUEST_PATH']
    send_file "./#{request.env['REQUEST_PATH']}"
  end
end

