ActionMailer::Base.raise_delivery_errors = true
ActionMailer::Base.delivery_method = :smtp

ActionMailer::Base.smtp_settings = {
  address: 'put real mail server here',
  port: put real port here,
  domain: 'put real domain here',
  user_name: 'put login username here',
  password: 'put password here',
  enable_starttls_auto: false
}

ActionMailer::Base.view_paths = File.join('./app', 'views')