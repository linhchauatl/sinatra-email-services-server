class VerificationMailer < ActionMailer::Base
  def send_mail(options)
    @comeback_url = options['comeback_url']
    @organization_name = options['organization_name']
    mail(to: options['to'], subject: options['subject'], from: 'Cisco-IoT<cisco-iot@cisco.com>').deliver!
  end
end