require 'spec_helper'

describe InvitationMailer do
  context 'send_mail' do
    before :all do
      @options = { 'to' => 'Test Recipient <test@nowhere.com>', 
                   'subject' => 'Hello, world!', 
                   'organization_name' => 'Awesome Organization',
                   'comeback_url' => 'http://www.cisco.com'
                 }
    end

    let(:mail) { InvitationMailer.send_mail(@options) }

    it 'renders the recipient' do
      expect(mail.to).to eql(['test@nowhere.com'])
    end

    it 'renders the subject' do
       expect(mail.subject).to eql('Hello, world!')
    end

    it 'renders the body' do
      expect(mail.body.raw_source).to match("<div style='color: green;'>\n\nThe organization 'Awesome Organization' invites you to participate in Cisco IoT platform.<br/>\nPlease click <a href='http://www.cisco.com'>here</a> to join the organization.\n\n</div>\n<br/>")
    end
  end
  
end