require 'spec_helper'

describe VerificationMailer do
  context 'send_mail' do
    before :all do
      @options = { 'to' => 'Test Recipient <test@nowhere.com>', 
                   'subject' => 'Hello, world!', 
                   'organization_name' => 'Awesome Organization',
                   'comeback_url' => 'http://www.cisco.com'
                 }
    end

    let(:mail) { VerificationMailer.send_mail(@options) }

    it 'renders the recipient' do
      expect(mail.to).to eql(['test@nowhere.com'])
    end

    it 'renders the subject' do
       expect(mail.subject).to eql('Hello, world!')
    end

    it 'renders the body' do
      expect(mail.body.raw_source).to match("<div style='color: blue;'>\n\nSome one registered organization 'Awesome Organization' with your email address.<br/>\nPlease click <a href='http://www.cisco.com'>here</a> to verify.\n\n</div>\n<br/>")
    end
  end
  
end