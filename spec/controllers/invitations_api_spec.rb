require 'spec_helper'

describe MyApp do
  context 'post /invitations.json' do
    before :all do
      @data = { organization_name: 'Awesome Organization', recipient: 'linhchau@cisco.com', comeback_url: 'http://www.cisco.com'}
    end

    it 'call InvitationMailer.send_mail if there are organization_name, recipient and comeback_url' do
      expect(InvitationMailer).to receive(:send_mail).and_return(Mail::Message.new)
      post '/invitations.json', @data.to_json
    end
  
  end
  
end