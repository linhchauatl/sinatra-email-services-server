require 'spec_helper'

describe MyApp do
  context 'post /verifications.json' do
    before :all do
      @data = { organization_name: 'Awesome Organization', recipient: 'linhchau@cisco.com', comeback_url: 'http://www.cisco.com'}
    end

    it 'call VerificationMailer.send_mail if there are organization_name, recipient and comeback_url' do
      expect(VerificationMailer).to receive(:send_mail).and_return(Mail::Message.new)
      post '/verifications.json', @data.to_json
    end
  
  end
  
end