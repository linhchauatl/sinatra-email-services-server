require 'spec_helper'

class FakeRequest
  attr_accessor :ssl, :env

  def initialize(ssl, env)
    self.ssl = ssl
    self.env = env
  end

  def ssl?
    self.ssl
  end
end


describe Swaggering do
  context 'self.base_path' do
    it 'returns http as protocol for non-ssl connection' do
      request = FakeRequest.new(false, { 'HTTP_HOST' => 'www.google.com' })
      expect(Swaggering.base_path(request)).to eql('http://www.google.com')
    end

    it 'returns https as protocol for ssl connection' do
      request = FakeRequest.new(true, { 'HTTP_HOST' => 'www.google.com' })
      expect(Swaggering.base_path(request)).to eql('https://www.google.com')
    end
  end
end
