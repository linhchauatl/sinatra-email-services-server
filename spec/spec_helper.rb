ENV['RACK_ENV'] = 'test'

require 'rspec'
require 'rack/test'
require './my_app'

# setup test environment
Bundler.setup(:default, :test)

RSpec.configure do |config|
  config.include Rack::Test::Methods

  config.color = true
  config.tty = true
  config.formatter = :documentation 
  
  def app
    described_class
  end
end

def color_print(args)
  Pry::ColorPrinter.pp args
end